#pragma once
#include "Spell.h"
//temporary
#include <iostream>
#include <string>

class FireRain : public Projectiles
{
public:
	std::string FireAsset;

	FireRain()
	{
		FireAsset = "Fire Rain upon you";
	}
	virtual void update(float dt)
	{
		std::cout << FireAsset << std::endl;
	}
};

class FireBall : public Projectiles
{
public:
	std::string FireAsset;

	FireBall()
	{
		FireAsset = "Fire ball shooting at you!";
	}
	virtual void update(float dt)
	{
		std::cout << FireAsset << std::endl;
	}
};

class FireBlast : public Projectiles
{
public:
	std::string FireAsset;

	FireBlast()
	{
		FireAsset = "Fire blast! you better hide dude...";
	}

	virtual void update(float dt)
	{
		std::cout << FireAsset << std::endl;
	}
};

/*
------------------------------------------------------

Static spells

------------------------------------------------------
*/


class FireFlies : public Statics
{
public:
	std::string Flies;
	bool IsEnemy;
	//Targeting component from UE4
	FireFlies()
	{
		IsEnemy = false;
		Flies = "Flies lighting up the sky";
	}

	virtual void update(float dt)
	{
		std::cout << Flies << std::endl;
	}
};

class IcePatch : public Statics
{
public:
	std::string IceImage;
	bool IsEnemy;
	//targeting component from UE4 
	IcePatch()
	{
		IsEnemy = false;
		IceImage = "icePatch slippery in the vincinity";
	}

	virtual void update(float dt)
	{
		std::cout << IceImage << std::endl;
	}
};

class Gravity : public Statics
{
public:
	std::string AreaOfEffect;
	Gravity()
	{
		//hard coded position to test the parent variable.
		m_Position[1][1][1] = 5.f;
		AreaOfEffect = "Gravitizing the shit out of this place!";
	}

	virtual void update(float dt)
	{
		std::cout << AreaOfEffect << std::endl;
	}
};


class FireWall : public Statics
{
public:
	std::string FireImage;
	FireWall()
	{
		FireImage = "You shall not pass! this fire wall";
	}

	virtual void update(float dt)
	{
		std::cout << FireImage << std::endl;
	}
};


class BigBang : public Statics
{
public:
	std::string BigBangImage;
	BigBang()
	{
		BigBangImage = "Big Bang !";
	}

	virtual void update(float dt)
	{
		std::cout << BigBangImage << std::endl;
	}
};


class BlackHole : public Statics
{
public:
	std::string BlackHoleImage;
	bool IsEnemy;
	//targeting compunenet from UE4
	BlackHole()
	{
		IsEnemy = false;
		BlackHoleImage = "Black hole baby";
	}

	virtual void update(float dt)
	{
		std::cout << BlackHoleImage << std::endl;
	}
};

/*
----------------------------------------------------------------------------

Misc

----------------------------------------------------------------------------

*/

class ShadowStep : public Misc
{
public:
	ShadowStep()
	{

	}
	virtual void update(float dt)
	{
		std::cout << "shadow step, moving shadowily to the side" << std::endl;
	}
};

class Threat : public Misc
{
public:
	Threat()
	{

	}
	virtual void update(float dt)
	{
		std::cout << "scaring the shit out of everybody" << std::endl;
	}
};