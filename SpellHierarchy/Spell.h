#pragma once
#include <iostream>


/*Base classes

These classes are the base classes
abstract classes only to provide an interface for the actual instances
of the spells.

Spell contain the update method that the instances found in SpellInstances.h
implement. These are not to be implemented in gameplay

they are ordered by mechanical qualities, such as if they are projectiles 
or static or otherwise. Misc spells move mokhtar or simply immobalizes enemies
somehow. 


NOTE:Do Spell need a position? 

*/
class Spell
{
public:
	int position[3][3][9];

	virtual void update(float) = 0;
};
class Misc : public Spell
{
protected:
	bool m_IsValid;
public:
	bool IsValid() { return m_IsValid; }
};
class Statics : public Spell
{
protected:
	float timer;
	float m_Position[1][1][1];
public:
	void updateTimer();
	float geTime() { return timer; }
	//a proper vector and no arguments requred
	float getPosition(int i , int j , int k) { return m_Position[i][j][k]; }
};
class Projectiles : public Statics
{
protected:
	float m_Velocity[1][1][1];
	bool m_IsValid;
public:
	bool IsValid() { return m_IsValid; }
};
