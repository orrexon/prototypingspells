#include "SpellInstances.h"

int main()
{
	FireBall fireBall;
	fireBall.update(1.0f);
	std::cout << std::endl;
	FireWall fireWall;
	fireWall.update(1.0f);
	std::cout << std::endl;
	FireFlies fireFlies;
	fireFlies.update(1.0f);
	std::cout << std::endl;
	IcePatch ice;
	ice.update(1.0f);
	std::cout << std::endl;
	Gravity grav;
	grav.update(.0f);
	std::cout << std::endl;
	std::cout << "gravity position = " << grav.getPosition(1, 1, 1) << std::endl;
	std::cout << std::endl;

	BigBang bigb;
	bigb.update(0.f);
	std::cout << std::endl;
	BlackHole blackh;
	blackh.update(.0f);
	std::cout << std::endl;
	FireRain frain;
	frain.update(0.f);
	std::cout << std::endl;
	FireBlast fblast;
	fblast.update(0.f);
	std::cout << std::endl;
	ShadowStep shstep;
	shstep.update(0.f);
	std::cout << std::endl;
	Threat threat;
	threat.update(0.f);
	return 0;
}